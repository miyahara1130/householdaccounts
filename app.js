'use strict'

// Angular
var app = angular.module('App', ['ngSanitize', 'ngRoute', 'ngMaterial', 'ngAnimate', 'ngMessages','ngStorage'])

app.value("Tasks", []);

// Angular Global Config
app.config(function($routeProvider) {
    // Angular Route
    $routeProvider.when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController'
    })
    .otherwise({
        redirectTo: '/'
    });

})

app.controller('HomeController', function($scope, Tasks, $localStorage) {
    //$localStorage.clear();
    //ローカルストレージからの読み出し
    if($localStorage.Tasks != [] && angular.isDefined($localStorage.Tasks)){
        Tasks = $localStorage.Tasks;
    }
    $scope.tasks = Tasks;

    //収入金額の計算
    var incomeSubTotal=0;
    angular.forEach(Tasks, function(value,idx,array){
        incomeSubTotal += value.incomeMoney;
    });
    $scope.incomeSum = incomeSubTotal;
    
     //支出金額の計算
    var spendingSubTotal =0;
    angular.forEach(Tasks, function(value,idx,array){
        spendingSubTotal += value.spendingMoney;
    });  
    $scope.spendingSum = spendingSubTotal;
    
    $scope.addIncomeHouseholdAccount = function(incomeDate,incomeExpress,incomeMoney) {
        //入力された項目を配列に格納
		Tasks.push({incomeDate: incomeDate,incomeExpress:incomeExpress, incomeMoney: incomeMoney,spendingDate:new Date,spendingExpress:0,spendingMoney:0,flag:true});
        //配列をローカルストレージに保存
        $localStorage.Tasks = Tasks;
        $scope.tasks = Tasks;
        Tasks = $localStorage.Tasks;
        
        //収入金額のリセット
        incomeSubTotal=0;
        
        //収入金額の計算
        angular.forEach(Tasks, function(value,idx,array){
            incomeSubTotal += value.incomeMoney;
            //ループの最後のタイミングで収入金額と収支金額のバインドを行う
            if(idx === array.length - 1){
                $scope.incomeSum = incomeSubTotal;
                $scope.total = incomeSubTotal - spendingSubTotal;
            }
        });
        
        $scope.incomeSum = incomeSubTotal;
	};
    
    $scope.addSpendingHouseholdAccount = function(spendingDate,spendingExpress,spendingMoney) {
        //入力された項目を配列に保存
		Tasks.push({incomeDate: new Date,incomeExpress:0, incomeMoney: 0,spendingDate:spendingDate,spendingExpress:spendingExpress,spendingMoney:spendingMoney,flag:false});
        //配列をローカルストレージに保存
        $localStorage.Tasks = Tasks;
        $scope.tasks = Tasks;
        Tasks = $localStorage.Tasks;
        
        //支出金額のリセット
        spendingSubTotal =0;
        //支出金額の計算
        angular.forEach(Tasks, function(value,idx,array){
            spendingSubTotal += value.spendingMoney;
            //ループの最後のタイミングで支出と収支のバインドを行う
            if(idx === array.length - 1){
                $scope.spendingSum = spendingSubTotal;
                $scope.total = incomeSubTotal - spendingSubTotal;
            }
        });
        
        $scope.spendingSum = spendingSubTotal;
	};
    
    //収入の削除ボタン処理
    $scope.deleteIncomeHouseholdAccount = function(i) {
		 angular.forEach(Tasks,function(value,idx){
            //削除ボタンを押した項目と支出として登録されている日時が一致したら
            //削除を実施する
            //ソートを行った関係で、$indexが使用できないためこういった処理を行った
            if(i==value.spendingDate){
                Tasks.splice(idx,1);
                incomeSubTotal = 0;
                //削除した結果、明細項目が0個になったら、0円を表示する
                if(Tasks.length == 0){
                    $scope.incomeSum = incomeSubTotal;
                    $scope.total = incomeSubTotal - spendingSubTotal;
                }
                //収入金額の再計算を行う
                angular.forEach(Tasks, function(value,idx,array){
                    incomeSubTotal += value.incomeMoney;
                    //ループの最後のタイミングでバインドを行う
                    if(idx === array.length - 1){
                        $scope.incomeSum = incomeSubTotal;
                        $scope.total = incomeSubTotal - spendingSubTotal;
                    }

                });
            }   
        });
		$scope.tasks = Tasks;
	}
    
    //支出の削除ボタン処理
    $scope.deleteSpendingHouseholdAccount = function(i) {
        angular.forEach(Tasks,function(value,idx){
            //削除ボタンを押した項目と収入として登録されている日時が一致したら
            //削除を実施する
            //ソートを行った関係で、$indexが使用できないためこういった処理を行った
            if(i==value.incomeDate){
                Tasks.splice(idx,1);
                spendingSubTotal =0;
                //削除した結果、明細項目が0個になったら、0円を表示する
                if(Tasks.length == 0){
                    $scope.spendingSum = spendingSubTotal;
                    $scope.total = incomeSubTotal - spendingSubTotal;
                }
                //支出金額の計算
                angular.forEach(Tasks, function(value,idx,array){
                    spendingSubTotal += value.spendingMoney;
                    //ループの最後のタイミングでバインドを行う
                    if(idx === array.length - 1){
                        $scope.spendingSum = spendingSubTotal;
                        $scope.total = incomeSubTotal - spendingSubTotal;
                    }
                });
            }   
        });
		$scope.tasks = Tasks;
	}
    
    $scope.total = incomeSubTotal - spendingSubTotal;
});

